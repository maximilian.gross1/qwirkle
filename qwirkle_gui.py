# -*- coding: utf-8 -*-
"""
Created on Sat Oct  7 18:17:02 2023

@author: Max
"""

import tkinter as tk
from tkinter import ttk
import sys
import time

import numpy as np
from tkinter import font
from PIL import ImageTk, Image
from qwirkle_game import QwirkleGame, Bag, Player, Bot



class Qwirkle_init_GUI:
    
    def __init__(self):
        
        # Inititate window
        self.window = tk.Tk()
        self.window.title("Qwirkle Initialization")
        self.window.geometry("400x400+560+140")
        
        self.label_font = font.Font(family="Helvetica", size=16)
        
        # Initiate properties
        self.name_entries = []
        self.name_list = []
        self.name_labels = []
        self.bot_bool = False

        self.players_confirmed = False
        self.bots_confirmed = False
        
        # Initiate methods
        self.bot_or_player()
        
        
        # Initiate mainloop
        self.window.mainloop()

    def bot_or_player(self):
        self.create_logo()
        self.bot_button = tk.Button(text="Play against Bots", command = lambda: self.bot_ui_first_step())#self.create_bot_ui())
        self.bot_button.place(x=200,y=200)

        self.player_button = tk.Button(text="Play against Players", command = lambda: self.create_ui_pvp())
        self.player_button.place(x=75,y=200)

    def bot_ui_first_step(self):
        self.bot_bool = True
        self.player_button.destroy()
        self.bot_button.destroy()

        # Choose and confirm number of bots
        self.num_of_bots_label = tk.Label(text='Select # of Bots:')
        self.num_of_bots_label.place(x=30,y=180)

        self.num_of_bots_combobox = ttk.Combobox(state="readonly", values=[1,2,3])
        self.num_of_bots_combobox.place(x=30,y=200)

        self.confirm_num_of_bots_button = tk.Button(text='Confirm # of Bots',command= lambda: self.bot_ui_intermediate_step(0))
        self.confirm_num_of_bots_button.place(x=30,y=230)

        # Choose and confirm number of real Players
        self.num_of_players_label = tk.Label(text='Select # of Players:')
        self.num_of_players_label.place(x=200,y=180)

        self.num_of_players_combobox = ttk.Combobox(state='readonly',values=[0,1,2])
        self.num_of_players_combobox.place(x=200,y=200)

        self.confirm_num_of_players_button = tk.Button(text='Confirm # of Players',command= lambda: self.bot_ui_intermediate_step(1))
        self.confirm_num_of_players_button.place(x=200,y=230)

    def bot_ui_intermediate_step(self,idx):

        if idx == 0:
            self.bots_confirmed = True
            self.num_of_bots = int(self.num_of_bots_combobox.get())

        if idx == 1:
            self.players_confirmed = True
            self.num_of_players = int(self.num_of_players_combobox.get())
            if self.num_of_players == 0:
                self.start_game()

        
         
        if self.bots_confirmed and self.players_confirmed:
            self.bot_ui_second_step()

        

    def bot_ui_second_step(self):

        self.num_of_bots_label.destroy()
        self.num_of_bots_combobox.destroy()
        self.confirm_num_of_bots_button.destroy()

        self.num_of_players_label.destroy()
        self.num_of_players_combobox.destroy()
        self.confirm_num_of_players_button.destroy()

        bot_title_label = tk.Label(text='Bots:')
        bot_title_label.place(x=30,y=150)
        for i in range(self.num_of_bots):
            this_bot_label = tk.Label(text=f'Bot {i+1}')
            this_bot_label.place(x=30,y=170+20*i)

        players_title_label = tk.Label(text='Players:')
        players_title_label.place(x=100,y=150)

        for i in range(self.num_of_players):
            entry = tk.Entry()
            entry.place(x=250,y=20*i+170,width=70)
            self.name_entries.append(entry)
            
            self.name_label = tk.Label(text=f"Enter Player {i+1} Name:")
            self.name_label.place(x=100,y=20*i+170)
            self.name_labels.append(self.name_label)
            self.confirm_button = tk.Button(text="Confirm Names", command= lambda: self.create_bot_start_game_button())
            self.confirm_button.place(x=240,y=220)


    """ def create_bot_ui(self):

        self.bot_bool = True
        self.player_button.destroy()
        self.bot_button.destroy()

        entry = tk.Entry()
        entry.place(x=130,y=200,width=100)
        self.name_entries.append(entry)
              
        self.name_label = tk.Label(text=f"Enter your Name:")
        self.name_label.place(x=30,y=200)
        self.name_labels.append(self.name_label)

        self.confirm_button = tk.Button(text="Confirm Names", command= lambda: self.create_bot_start_game_button())
        self.confirm_button.place(x=240,y=197) """

    def create_bot_start_game_button(self):
        
        self.confirm_button.config(text="Start Game", bg="spring green", command= lambda: self.start_game())
        self.confirm_button.place(x=240,y=250)
        for i in range(len(self.name_entries)):
            self.name_list.append(self.name_entries[i].get())
   


    def create_ui_pvp(self):
        self.player_button.destroy()
        self.bot_button.destroy()
        self.pvp_comboboxes()
        self.pvp_buttons()
        self.pvp_labels()

    def pvp_buttons(self):  
        
        # start_game_button = tk.Button(width=20, height=10,bg="green",text="START")
        # start_game_button.place(x=300,y=300)
        
        self.confirm_button = tk.Button(text="Confirm # of players", command = lambda: self.pvp_create_player_names())
        self.confirm_button.place(x=180,y=197)
        
    def create_logo(self):
        self.logo_image = Image.open("logo.jpg")
        self.logo_image = self.logo_image.resize((200,100),Image.LANCZOS)
        self.logo_image = ImageTk.PhotoImage(self.logo_image)
        
        logo_label = tk.Label(image=self.logo_image)
        logo_label.place(x=50,y=10)

    def pvp_labels(self):
        
        self.create_logo()
        
        self.header = tk.Label(text = "Select number of Players", font=self.label_font)
        self.header.place(x=30,y=150)
                
    def pvp_comboboxes(self):
    
        self.select_num_of_players = ttk.Combobox(state="readonly", values=[1,2,3,4])
        self.select_num_of_players.place(x=30,y=200)
                
    def pvp_create_player_names(self):
                
        self.num_of_players = int(self.select_num_of_players.get())
        
        for i in range(self.num_of_players):
            
            entry = tk.Entry()
            entry.place(x=150,y=50*i+200,width=70)
            self.name_entries.append(entry)
            
            self.name_label = tk.Label(text=f"Enter Player {i+1} Name:")
            self.name_label.place(x=30,y=50*i+200)
            self.name_labels.append(self.name_label)
        
        self.select_num_of_players.destroy()
        
        self.confirm_button.config(text="Confirm Names", command= lambda: self.create_start_game_button()) 
        self.confirm_button.place(x=223,y=153)
        self.header.config(text="Enter player names")
        
                    
    def create_start_game_button(self):
        
        # self.confirm_button.destroy()
        
        for i in range(len(self.name_entries)):
            self.name_list.append(self.name_entries[i].get())
            self.name_entries[i].destroy()
            self.name_labels[i].config(text=f"Player {i+1} Name: {self.name_list[i]}")
        
        self.header.config(text="Click Start Game:")
        
        self.confirm_button.config(text="Start Game", bg="spring green", command= lambda: self.start_game())
        self.confirm_button.place(x=200,y=153)
        
    def start_game(self):
        self.window.destroy()
        qwirkle_gui = QwirkleGUI(self.name_list,self.bot_bool,self.num_of_bots)
        
        
        
class QwirkleGUI():
    
    def __init__(self,name_list,bot_bool,num_of_bots):
        
        root = tk.Tk()
        root.title('QWIRKLE')
        root.withdraw()

        self.bot_bool = bot_bool

        self.name_list = name_list
        self.num_of_bots = num_of_bots
        
        
        window = tk.Tk()
        self.screen_width = window.winfo_screenwidth() -10
        self.screen_height = window.winfo_screenheight() -30
        window.destroy()

        self.windows = []

        def on_closing(window):
            sys.exit()

        for i in range(len(self.name_list)):
            window = tk.Toplevel(root)
            window.geometry(f"{self.screen_width}x{self.screen_height}-{1922*i}+0")
            window.protocol('WM_DELETE_WINDOW',lambda: on_closing(window))
            self.windows.append(window)
        

        if len(self.windows) == 0:
            window = tk.Toplevel(root)
            window.geometry(f"{self.screen_width}x{self.screen_height}-{1918}+0")
            window.protocol('WM_DELETE_WINDOW',lambda: on_closing(window))
            self.windows.append(window)


        self.label_font = font.Font(family="Helvetica", size=16)
        
        self.qwirkle_game = QwirkleGame()
        
        #self.board = self.qwirkle_game.create_test_board()
        self.board = np.zeros((1,1))
        self.bag = Bag()
        
        
        self.players = []
        self.create_player_list()
        
              
        self.selected_tile = None 
        self.turn_memory = []

        self.bag_labels = []
        self.bot_score_labels = []
        self.opponent_score_labels = []

        for i in range(len(self.windows)):
            bag_label = tk.Label(self.windows[i],text=f"Tiles left in bag: {len(self.bag.tiles)}",font=self.label_font)
            bag_label.place(x=10,y=10)
            self.bag_labels.append(bag_label)
        
        self.end_turn_button()
        
        self.all_images = self.load_all_images(55)
        
        self.bench_images = self.load_all_images(70)
        
        self.bench_tiles = []
        self.bench_image_list = []
        self.bench_buttons = []
        
        self.active_player = None
        self.active_button = None
        self.active_button_index = None
        self.player_score_labels = []

        self.tiles_left_labels = []
        
        

        self.board_image_list = []
        self.board_button_list = []
        
        self.button_size = None

        if bot_bool:
            self.initiate_bot()
        
        self.create_bench(bot_bool)

        self.create_board()

        root.mainloop()

    def create_player_list(self):
        for i in range(len(self.name_list)):
            self.players.append(Player(self.name_list[i],i))
        
    
    def initiate_bot(self):
        for i in range(self.num_of_bots):    
            self.players.append(Bot(f'Bot {i+1}', self.qwirkle_game,i))
            for j in range(len(self.windows)):
                this_label = tk.Label(self.windows[j],text=f'Bot {i+1} has {6} tiles on hand',font=self.label_font)
                this_label.place(x=500+500*i,y=10)
                self.tiles_left_labels.append(this_label)
        

    
    # Methods that handle bench tiles.
    def create_bench(self,bot_bool):
        
        if bot_bool:
            
            for j in range(len(self.players)):
                for i in range(6):

                    
                    tile = self.bag.draw_tile()
                    self.players[j].hand.append(tile)
                    self.bench_tiles.append(tile)

                    border = 10
                    
                    if not self.players[j].is_bot: # If current player is not the bot: create buttons and images for hand
                        image = self.bench_images[tile]
                        image = ImageTk.PhotoImage(image)
                        
                        # self.bench_image_list.append(image)
                        self.players[j].image_list.append(image)
                        
                        
                        
                        # Calculating bench positions depending on player count
                        #if j == 0:
                        position_x = 80*i + self.screen_width/2 - 80*3 
                        position_y = self.screen_height - border - 120
            
                        # Creating the buttons and placing them, also storing them in bench_buttons list.
                        bench_button = tk.Button(self.windows[j],width=75, height=75, image=image, command=lambda i=i, j=j: self.on_bench_button_click(i,j))
                        bench_button.place(x=position_x, y=position_y)
                        #self.bench_buttons.append(bench_button)
                        self.players[j].bench_buttons.append(bench_button)
                        
                # Bench Labels
                # Calculating player name label position
              
                position_x = self.screen_width/2 - 3*80 
                position_y = self.screen_height - border - 150

                if self.players[j].is_bot:
                # Placing player name labels
           
                    for k in range(len(self.windows)):
                        # Score labels
                        bot_score_label = tk.Label(self.windows[k],text=f"Bot {self.players[j].id+1} Score: {self.players[j].score}",font=self.label_font)
                        bot_score_label.place(x=500*(self.players[j].id+1),y=40)
                        self.bot_score_labels.append(bot_score_label)

             
                # Placing player name labels
                if not self.players[j].is_bot:
                   
                    player_name_label = tk.Label(self.windows[j],text=f"Player: {self.players[j].name}", font=self.label_font) 
                    player_name_label.place(x=position_x, y=position_y)
               
                    # Score labels
                    player_score_label = tk.Label(self.windows[j],text=f"Score: {self.players[j].score}",font=self.label_font)
                    player_score_label.place(x= position_x+250,y=position_y)
                    self.player_score_labels.append(player_score_label)

                    if len(self.windows) != 1:
                        if self.players[j].id == 0:
                            opponent_score_label = tk.Label(self.windows[1],text=f"Player {self.players[j].name} Score: {self.players[j].score}, Tiles left: {len(self.players[j].hand)}",font=self.label_font)
                            opponent_score_label.place(x=500,y=80)
                            self.opponent_score_labels.append(opponent_score_label)
                            

                        elif self.players[j].id ==1:
                            opponent_score_label = tk.Label(self.windows[0],text=f"Player {self.players[j].name} Score: {self.players[j].score}, Tiles left: {len(self.players[j].hand)}",font=self.label_font)
                            opponent_score_label.place(x=500,y=80)
                            self.opponent_score_labels.append(opponent_score_label)
                    
                 
            self.active_player = 0
            
            for j in range(len(self.players)):
                if j != self.active_player and not self.players[j].is_bot:
                    for i in range(6):
                        self.players[j].bench_buttons[i].config(state="disabled")#, image = ImageTk.PhotoImage(self.bench_images[0]))
            

        else:
            # This method creates the buttons that are representing the active players hand and assigns the button images according to the tiles.
            # Buttons get a callback assigned and are then placed in the window using the place() method.
        
            # Bench Creation
            for j in range(len(self.players)):
                for i in range(6):
                    
                    tile = self.bag.draw_tile()               
                        
                    self.players[j].hand.append(tile)
                    self.bench_tiles.append(tile)
                    
                    image = self.bench_images[tile]
                    image = ImageTk.PhotoImage(image)
                    
                    # self.bench_image_list.append(image)
                    self.players[j].image_list.append(image)
                    
                    border = 10
                    
                    # Calculating bench positions depending on player count
                    if j == 0:
                        position_x = 80*i + self.screen_width/2 - 80*3 
                        position_y = self.screen_height - border - 120
                    
                    elif j == 1:
                        position_x = 80*i + self.screen_width/2 - 80*3 -700
                        position_y = self.screen_height - border - 80
                        
                    elif j == 2:
                        position_x = border
                        position_y = 80*i + self.screen_height/2 - 80*3
                        
                    elif j == 3:
                        position_x = self.screen_width - border - 80
                        position_y = 80*i + self.screen_height/2 - 80*3

                    # Creating the buttons and placing them, also storing them in bench_buttons list.
                    bench_button = tk.Button(width=75, height=75, image=image, command=lambda i=i, j=j: self.on_bench_button_click(i,j))
                    bench_button.place(x=position_x, y=position_y)
                    #self.bench_buttons.append(bench_button)
                    self.players[j].bench_buttons.append(bench_button)
                    
                
                
                # Bench Labels
                # Calculating player name label position
                if j == 0:
                    position_x = self.screen_width/2 - 3*80 
                    position_y = self.screen_height - border - 150
                    
                elif j == 1:
                    position_x = self.screen_width/2 - 3*80 -200
                    position_y = self.screen_height - border - 80 - 30
                    
                elif j == 2:
                    position_x = 10
                    position_y = 270
                
                elif j == 3:
                    player_name = str(self.players[j].name)
                    
                    position_x = 1828 - 12 * len(player_name)
                    position_y = 270
                    
                # Placing player name labels
                player_name_label = tk.Label(text=f"Player: {self.players[j].name}", font=self.label_font) 
                player_name_label.place(x=position_x, y=position_y)
                
                player_score_label = tk.Label(text=f"Score: {self.players[j].score}",font=self.label_font)
                player_score_label.place(x= position_x+100,y=position_y)
                
                self.player_score_labels.append(player_score_label)
                
            self.active_player = 0
            
            for j in range(len(self.players)):
                if j != self.active_player:
                    for i in range(6):
                        self.players[j].bench_buttons[i].config(state="disabled")#, image = ImageTk.PhotoImage(self.bench_images[0]))
        
        
    def end_turn_button(self):
        for i in range(len(self.windows)):
            end_turn_button = tk.Button(self.windows[i],text = "End Turn", command= lambda: self.end_turn())
            end_turn_button.place(x=1800,y=0)

        



    ### NEW
    global t00
    t00 = time.time()

    def end_turn(self):
        flag = False
        
        if not self.bot_bool:   
            for i in range(6):
                
                if self.players[self.active_player].hand[i] == 0:
                    
                    try:
                        tile = self.bag.draw_tile()
                        
                    except:
                        tile = 0
                        flag = True
                        # print("Bag is empty")
                                        
                    self.players[self.active_player].hand[i] = tile
                    
                    image = self.bench_images[tile]
                    image = ImageTk.PhotoImage(image)
                    
                    self.players[self.active_player].image_list[i] = image
                    self.players[self.active_player].bench_buttons[i].config(image=image)
                    
                self.players[self.active_player].bench_buttons[i].config(state="disabled") #, image = ImageTk.PhotoImage(self.bench_images[0]))
            

            turn_score = self.qwirkle_game.calculate_score(self.board, self.turn_memory)
            self.players[self.active_player].score += turn_score
            
            self.player_score_labels[self.active_player].config(text=f"Score: {self.players[self.active_player].score}, Last move scored: {turn_score}")
            
            bools = [0,0,0,0,0,0]
            
            for i in range(6):
                if self.players[self.active_player].hand[i] == 0:
                    bools[i] = True
                
            
            if flag and all(bools):
                self.players[self.active_player].score+=6
                best_player = max(self.players, key=lambda player: player.score)
                winner_name = best_player.name
                for player in self.players:
                        if player is not best_player:
                                looser = player
                print(f"The winner is {winner_name} with {best_player.score} points.")
                print(f"The looser is {looser.name} with {looser.score} points.")
                #self.window.destroy()
                sys.exit()
                
                

            self.active_player += 1
            if self.active_player == len(self.players):
                self.active_player = 0

            for i in range(6):
                self.players[self.active_player].bench_buttons[i].config(state="active") 
            
            self.bag_label.config(text=f"Tiles left in bag: {len(self.bag.tiles)}")
            

        if self.bot_bool:
            if self.players[self.active_player].is_bot:
               
                this_bot = self.players[self.active_player]

                t0 = time.time()
                self.board,self.players[self.active_player].hand,bot_score,bot_moves = this_bot.parallel_move_search(self.board,this_bot.hand)
                t1 = time.time()

                self.players[self.active_player].total_thinking_time += t1-t0

                self.create_board(bot_moves)
                
                for i in range(6-len(this_bot.hand)):
                    this_bot.hand.append(0)       

                for i in range(6):
                    
                    if self.players[self.active_player].hand[i] == 0:
                        
                        try:
                            tile = self.bag.draw_tile()
                            
                        except:
                            tile = 0
                            flag = True
                            
                        self.players[self.active_player].hand[i] = tile
    
                self.players[self.active_player].score += bot_score
                
                id = this_bot.id

                n = 0
                for tile in self.players[self.active_player].hand:
                    if tile!=0:
                        n+=1
                

                if len(self.windows) != 1:
                    if id == 0:
                        rangemap = [0,1]
                    if id == 1:
                        rangemap=[2,3]
                    if id == 2:
                        rangemap=[9,6]

                    for i in rangemap:
                        self.bot_score_labels[i].config(text=f"Bot {self.players[self.active_player].id+1} Score: {self.players[self.active_player].score}, Last move scored: {bot_score}")
                        self.tiles_left_labels[i].config(text=f"Bot {id+1} has {n} tiles on hand")
                else:
                      
                    self.bot_score_labels[id].config(text=f"Bot {self.players[self.active_player].id+1} Score: {self.players[self.active_player].score}, Last move scored: {bot_score}")        
                    self.tiles_left_labels[id].config(text=f"Bot {id+1} has {n} tiles on hand")
                
                
                
                bools = [0,0,0,0,0,0]
                
                for i in range(6):
                    if self.players[self.active_player].hand[i] == 0:
                        bools[i] = True
                    
                
                if flag and all(bools):
                    self.players[self.active_player].score+=6
                    all_t = 0
                    for player in self.players:
                        print(f'{player.name} scored {player.score}')
                        if player.is_bot:
                            print(f'Thinking time: {player.total_thinking_time}')
                            all_t += player.total_thinking_time
                    #self.window.destroy()
                    print(f'Total thinking time: {all_t}')
                    print(f'Game Time: {time.time()-t00}')
                    sys.exit()
                    
                
                self.active_player += 1
                if self.active_player == len(self.players):
                    self.active_player = 0

                if not self.players[self.active_player].is_bot:
                    for i in range(6):
                        self.players[self.active_player].bench_buttons[i].config(state="active")
               

                for i in range(len(self.windows)):
                    self.bag_labels[i].config(text=f"Tiles left in bag: {len(self.bag.tiles)}")

                
          
          


            else:
           
                for i in range(6):
                
                    if self.players[self.active_player].hand[i] == 0:
                    
                        try:
                            tile = self.bag.draw_tile()
                        
                        except:
                            tile = 0
                            flag = True

                        self.players[self.active_player].hand[i] = tile
                    
                        image = self.bench_images[tile]
                        image = ImageTk.PhotoImage(image)
                    
                        self.players[self.active_player].image_list[i] = image
                        self.players[self.active_player].bench_buttons[i].config(image=image)
                    
                turn_score = self.qwirkle_game.calculate_score(self.board, self.turn_memory)
                self.players[self.active_player].score += turn_score
            
                self.player_score_labels[self.active_player].config(text=f"Score: {self.players[self.active_player].score}, Last move scored: {turn_score}")

                n = 0
                for tile in self.players[self.active_player].hand:
                    if tile!=0:
                        n+=1

                if len(self.windows) != 1:
                    if self.active_player == 1:
                        idx = 1
                        self.opponent_score_labels[idx].config(text=f"Player {self.players[self.active_player].name} Score: {self.players[self.active_player].score}, Last move scored: {turn_score}, Tiles left: {n}")
                    
                    elif self.active_player == 0:
                        idx = 0
                        self.opponent_score_labels[idx].config(text=f"Player {self.players[self.active_player].name} Score: {self.players[self.active_player].score}, Last move scored: {turn_score}, Tiles left: {n}")



                for i in range(len(self.windows)):
                    self.bag_labels[i].config(text=f"Tiles left in bag: {len(self.bag.tiles)}")

            
                bools = [0,0,0,0,0,0]
                
                for i in range(6):
                    if self.players[self.active_player].hand[i] == 0:
                        bools[i] = True
                    
                
                if flag and all(bools):
                    self.players[self.active_player].score+=6
                    
                    for player in self.players:
                        print(f'{player.name} scored {player.score}')
                    #self.window.destroy()
                    sys.exit()
                
                for i in range(6):
                        self.players[self.active_player].bench_buttons[i].config(state="disabled")
                          
                self.active_player+=1
                if self.active_player == len(self.players):
                    self.active_player = 0

                self.turn_memory=[]
         

              

        self.wait_for_move()

        self.turn_memory = []

        

    
    def wait_for_move(self):
  
        if self.players[self.active_player].is_bot:
            #pass
            self.end_turn()
        
        else:
            for i in range(6):
                    self.players[self.active_player].bench_buttons[i].config(state="active")
            
     

    def on_bench_button_click(self,i,j):
        # This method handles the bench button callbacks. 
        self.active_player = j
        self.active_button_index = i
        
        try:
            self.active_button.config(bg="white")
        except:
            pass
        
        # Set the selected buttons bg color to red (i.e. display its selected). Set active_button to the selected button for if statement above. 
        # Set selected_tile to to the tile on the selected button by accessing bench_tiles list at the index of the selected button.
        
        self.players[j].bench_buttons[i].config(bg="red")
        self.active_button = self.players[j].bench_buttons[i]

        
        
        self.selected_tile = self.players[j].hand[i]
        
    def update_bench_buttons(self):
        # This method update the displayed bench_buttons to stop displaying a button when its been placed.
        
        i = self.active_button_index
        
        # Identify the button by index.
        button = self.players[self.active_player].bench_buttons[i]
        
        # Getting image for empty button and storing it in bench_image_list.
        # image = Image.open("graphics\\0.jpg")
        # image = image.resize((70,70),Image.ANTIALIAS)
        # image = ImageTk.PhotoImage(image)
        
        image = self.bench_images[0]
       
        image = ImageTk.PhotoImage(image)
        self.players[self.active_player].image_list[i] = image
        
        # Configuring button to display empty image and setting bg color to white (i.e. to display no longer selected).    
        button.config(image=image,bg="white")
        
        # Setting selected tile to None, since tile has been played.
        self.selected_tile = 0
        self.players[self.active_player].hand[self.active_button_index] = 0
        
    
    def resize_all_images(self,board_height,board_width):

        for i in range(len(self.all_images)):
            try:  
                self.all_images[i] = self.all_images[i].resize((self.button_size-5,self.button_size-5), Image.ANTIALIAS)
                
            except:
                self.all_images[i] = None
        
        # return self.all_images
    
    
    def load_all_images(self,size):
        
        all_images = []
        
        for i in range(67):
            try:
                i_str = str(i)
               
                image = Image.open("graphics\\" + i_str + ".jpg")
                image = image.resize((size,size), Image.ANTIALIAS)
                # image = ImageTk.PhotoImage(image)
                
            except:
                image = None
            
            all_images.append(image)
        
 
        print("all images loaded")
        return all_images
    
    # Methods that handle board tiles
    ### NEW
    def create_board(self,*args):

        # This method creates the board.
        for i in range(len(self.board_button_list)):
            self.board_button_list[i].destroy()
        
        self.board_image_list = []
        self.empty_image_list = []
        
        # Assign board_size and initalize a counter.
        board_height = self.board.shape[0]
        board_width = self.board.shape[1]
        
        
        if board_height < 11 and board_width < 25:
            self.button_size = 70

        elif board_height > 10:
            
            self.button_size = 700 // board_height + board_height//2 - 10
            
            self.resize_all_images(board_height, board_width)
            
        elif board_width > 24:
            self.button_size = 70 - (board_width - 22)*4
            self.resize_all_images(board_height, board_width)
            
        x_center = (1920 - self.button_size * board_width) / 2
        y_center = (1080 - self.button_size * board_height) / 2 

        
        # Loop through the width and height of the board.
        for i in range(board_height): 
            for j in range(board_width):  

                # Assign the tile on current loop iteration to current_tile.
                current_tile = self.board[i,j]
                
                if current_tile != 0:            
                    image = self.all_images[int(current_tile)]
                    image = ImageTk.PhotoImage(image)
                    self.board_image_list.append(image)
                    
                else:
                    image = tk.PhotoImage(width=1, height=1)
                    self.empty_image_list.append(image)
         
          
                # Create board button at the current iteration position and configure current tile image to display it.
                # Assign callback to individual buttons by passing counter to callback.

                for k in range(len(self.windows)):
                    board_button = tk.Button(self.windows[k],width = self.button_size, height = self.button_size, image=image, 
                                            command= lambda i=i, j=j: self.on_board_button_click(i,j))
                    
                    board_button.place(x = x_center + self.button_size * j, y = y_center + self.button_size * i) 
                    self.board_button_list.append(board_button)

                    if args:                 
                        moves = args[0]             
                        for move in moves:
                            if i == move[1] and j == move[2]:
                                board_button.config(bg='lightgreen')


    def on_board_button_click(self,i,j):
        # This method handles the board buttons callbacks.
   
        
        if self.qwirkle_game.is_valid_move(self.board,self.selected_tile,(i,j)) :
            is_valid = True
            
        else:
            is_valid = False


        is_valid_flow = self.qwirkle_game.additonal_game_flow_check(self.board,self.selected_tile,i,j,self.turn_memory)
        
        if is_valid and is_valid_flow:
            is_valid = True
        else:
            is_valid = False
    
      
        if is_valid:
            # In case the checks above all return that the move is valid, usual image processing and assigning is done as well as the memory management.
            
            self.board[i, j] = self.selected_tile
            
            # Add move to turn memory for multi move checks.
            self.turn_memory.append([self.selected_tile,i,j])
            # Upsize the gameboard if necessary.
      
            self.board,self.turn_memory = self.qwirkle_game.upsize_board(self.board,self.selected_tile,self.turn_memory)
            # Update the bench buttons if necessary.
            self.update_bench_buttons()
            # Create the new updated board
            self.create_board()
            
        
        else:
            # If the move is invalid display that.
            print("Illegal Move")
    

if __name__ == "__main__":
    qwirkle_gui = Qwirkle_init_GUI()
        
        
