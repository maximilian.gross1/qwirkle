# -*- coding: utf-8 -*-
"""
Created on Sun Oct 22 16:41:26 2023

@author: Max
"""
import numpy as np
from qwirkle_game import Player, QwirkleGame
from qwirkle_gui import QwirkleGUI

class Bot(Player):


    def __init__(self,name,qwirkle_game):
        super().__init__(name)

        
        self.game = qwirkle_game


        self.all_move_combinations = []

        self.depth = 0
        self.master_list = []
 

        self.move_combinations = []

        self.turn_memory = []
        

    def get_first_moves(self,board,hand):

        first_moves = []

        m = board.shape[0]
        n = board.shape[1]
        
        for i in range(m):
            for j in range(n):
                for k in range(len(hand)):

                    if QwirkleGame.is_valid_move(self.game,board,hand[k],(i,j)):    
                        
                        if QwirkleGame.additonal_game_flow_check(self.game,board,hand[k],i,j,self.turn_memory):
                                first_moves.append([hand[k],i,j])
                        
                        elif len(self.turn_memory) == 0:
                            first_moves.append([hand[k],i,j])
       
        return first_moves

    def make_move(self,board,move,tiles):
        

        tile,i,j = move[0],move[1],move[2]

     
        self.turn_memory.append(move)
        ## ADD removal from HAND
        
        for idx,piece in enumerate(tiles):
            if piece == tile:
                tiles.pop(idx)
                break

            

        board[i,j] = tile
            
        board,self.turn_memory = QwirkleGame.upsize_board(self.game,board,tile,self.turn_memory)            
                
        return board,tiles
    
    def frfr(self,board,hand):
        
        i = 0
        
        
        a_idx = 0
        combos = []

        first_moves = self.get_first_moves(board,hand)
     

        while True:
        
            i+=1
            print(i)
            
            tiles = hand
            b_idx = 0
            
            try:
                print('A')
                
                
                self.turn_memory = []
                this_move = first_moves[a_idx]
                board,tiles = self.make_move(board,this_move,tiles)
                a_idx += 1
                print(board)
          
            except IndexError:
                
                #board,tiles = self.undo_last_move(board,self.turn_memory[-1],tiles)
                
                
                break

            while True:
                i+=1
                print(i)
                c_idx = 0
                b_moves = self.get_first_moves(board,tiles)
                try:
                    print('B')
                    this_b_move = b_moves[b_idx]
                    board,tiles = self.make_move(board,this_b_move,tiles)
                    b_idx += 1
                    print(board)
                  
                except IndexError:
                    print(i)
                    print(self.turn_memory)
                    
                        
                    board,tiles = self.undo_last_move(board,self.turn_memory[-1],tiles)
                    
                    print('pre downsize')
                    print(board)
                    board = self.downsize_board(board)
                    print('post downsize')
                    print(board)
                    
                    break    



    def create_static_board(self,input_board):

        m,n = input_board.shape

        for i in range(4):
            input_board = np.vstack((input_board,np.zeros((1,n))))
            input_board = np.vstack(((np.zeros((1,n))),input_board))

        m,n = input_board.shape
       

        for i in range(4):
            input_board = np.hstack((input_board,np.zeros((m,1))))
            input_board = np.hstack(((np.zeros((m,1))),input_board))


        return input_board
        
        
    # DONE: new idea: work with static board size by 5 rows and 5 columns of zeros in all directions                

    # TODO
    # last move in turn memory has to be popped in undo_last_move() ?! -> has to be done like in case B!
    # change turn memory in cases a-f
    # calculate next letter moves at the end of letter ahead, like b_moves calculated in a
    # turn memory positions are wrong because of up and downsizing of board
    
    def get_best_move(self,input_board,hand):

        best_moves = None
        best_score = 0
        
        working_board = self.create_static_board(input_board)
    
        a_idx = 0
        first_moves = self.get_first_moves(working_board,hand)

        while True:
            tiles = hand
            b_idx = 0
            
            try:
                self.turn_memory = []
                this_move = first_moves[a_idx]
                
                working_board,tiles = self.make_move(working_board,this_move,tiles)
                a_idx += 1

                b_moves = self.get_first_moves(working_board,tiles)
                
          
            except IndexError:
                this_score = self.game.calculate_score(working_board,self.turn_memory)
                if this_score > best_score:
                    best_moves = self.turn_memory.copy()
                    best_score = this_score

                break

            while True:
                c_idx = 0
                try:   
                    this_b_move = b_moves[b_idx]
                    working_board,tiles = self.make_move(working_board,this_b_move,tiles)
                    b_idx += 1

                    c_moves = self.get_first_moves(working_board,tiles)
                    
                except IndexError:
                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                    if this_score > best_score:
                        best_moves = self.turn_memory.copy()
                        best_score = this_score

                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                    self.turn_memory.pop()       

                    break

                while True:
                    d_idx = 0
                    try:
                        this_c_move = c_moves[c_idx]
                        working_board,tiles = self.make_move(working_board,this_c_move,tiles)
                        c_idx += 1

                        d_moves = self.get_first_moves(working_board,tiles)
                            
                    except IndexError:
                        this_score = self.game.calculate_score(working_board,self.turn_memory)
                        if this_score > best_score:
                            best_moves = self.turn_memory.copy()
                            best_score = this_score

                        working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                        self.turn_memory.pop()
                        break

                    while True:
                        e_idx = 0
                        try:       
                            this_d_move = d_moves[d_idx]
                            working_board,tiles = self.make_move(working_board,this_d_move,tiles)
                            d_idx += 1

                            e_moves = self.get_first_moves(working_board,tiles)
                                
                        except IndexError:
                            this_score = self.game.calculate_score(working_board,self.turn_memory)
                            if this_score > best_score:
                                best_moves = self.turn_memory.copy()
                                best_score = this_score

                            working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                            self.turn_memory.pop()

                            break

                        while True:
                            f_idx = 0
                            try:
                                this_e_move = e_moves[e_idx]
                                working_board,tiles = self.make_move(working_board,this_e_move,tiles)
                                e_idx += 1

                                f_moves = self.get_first_moves(working_board,tiles)
                                    
                            except IndexError:
                                this_score = self.game.calculate_score(working_board,self.turn_memory)
                                if this_score > best_score:
                                    best_moves = self.turn_memory.copy()
                                    best_score = this_score

                                working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                self.turn_memory.pop()

                                break

                            while True:
                                try: 
                                    this_f_move = f_moves[f_idx]
                                    working_board,tiles = self.make_move(working_board,this_f_move,tiles)
                                    f_idx += 1
                                        
                                except IndexError:
                                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                                    if this_score > best_score:
                                        best_moves = self.turn_memory.copy()
                                        best_score = this_score

                                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                    self.turn_memory.pop()
                                    break
        
        print(best_score,best_moves)                            
        return best_score, best_moves
                        


        
    def legal(self,move):
        legal = False
        if QwirkleGame.is_valid_move(self.game,move[0],(move[1],move[2])):

            if self.turn_memory:
                if QwirkleGame.additonal_game_flow_check(self.game,move[0],move[1],move[2],self.turn_memory):
                    legal = True
            else:
                legal = True
        return legal
    


    def undo_last_move(self,board,move,tiles):
        tile,i,j = move
        board[i,j] = 0
        tiles.append(tile)
        return board,tiles

    def downsize_board(self,board):

        m = board.shape[0]
        n = board.shape[1]
     
        count_row_asc = 0
        count_row_desc = 0

        count_col_asc = 0
        count_col_desc = 0
        
        for row in range(m):
            row -= count_row_asc
            try:
                if np.all(board[row,:]==0) and np.all(board[row+1,:]==0):
                    board = np.delete(board,row,0)
                    count_row_asc += 1            
                else:
                    break
            except IndexError:
                print('row +')
                break

        for col in range(n):
            col -= count_col_asc
            try:
                if np.all(board[:,col]==0) and np.all(board[:,col+1]==0):
                    board = np.delete(board,col,1)
                    count_col_asc += 1
                    
                else:
                    break
            except IndexError:
                print('col +')
                break

        m = board.shape[0]
        n = board.shape[1]

        for row in reversed(range(m)):
            row -= count_row_desc
            try:
                if np.all(board[row,:]==0) and np.all(board[row-1,:]==0):
                    board = np.delete(board,row,0)
                    count_row_desc += 1
                                
                else:
                    break 
            except IndexError:
                print('row -')
                break

        for col in reversed(range(n)):
            col -= count_col_desc
            try:
                if np.all(board[:,col]==0) and np.all(board[:,col-1]==0):
                    board = np.delete(board,col,1)
                    count_col_desc += 1
                    
                else:
                    break
            except IndexError:
                print('col -')
                break
        
        return board

 
            
        

        

   
            

            
            
            
            
            
           





    
                            
                    
        
        
        
