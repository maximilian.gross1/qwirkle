# -*- coding: utf-8 -*-
"""
Created on Sat Oct  7 14:27:35 2023

@author: Max
"""


import random
import multiprocessing
import numpy as np
import pp




class QwirkleGame:
    def __init__(self):
        # Initialize the game state, such as the board, player hands, and score.

        self.board = np.zeros((1,1))  # 2D list representing the game board.
 
        self.players = []  # List of player objects.
        self.current_player = None  # Track the current player.


    def create_test_board(self):
        # Create predefined game state on board
        board = np.zeros((2,2))
        return board

    def start_game(self, num_players):
        # Initialize the players and bag?

        self.bag = Bag()
        
        #Initialize players
        for i in range(num_players):
            player = Player(f"Player {i + 1}")
            player.hand = self.deal_tiles(player,6)
            self.players.append(player)
            
        # Set up the board (empty for now)
        
        # Set the current player (first one for now)
        self.current_player = self.players[0]
        print("Game has started with", num_players, "players.")    
            
    def deal_tiles(self, player):
        # Deal tiles to players
        num_tiles_to_deal = 6 - len(player.hand)
        
        if num_tiles_to_deal > len(self.bag.tiles):
            num_tiles_to_deal = len(self.bag.tiles)
        
        for i in range(num_tiles_to_deal):
            tile = self.bag.draw_tile()
            player.hand.append(tile)
        
    
    def make_move(self, player, tile, position):
        # Handle a player's move (placing a tile on the board).
        # Check for validity, update the board, and calculate scores.
        if self.is_valid_move(tile,position):
            self.board[position] = tile
            move_score = self.calculate_score(position)
            player.score = player.score + move_score
            player.hand.remove(tile)
            self.switch_turn()
        

    def is_valid_move(self,board, tile, position):
        # Check if a single placed piece is valid at the given position, considering Qwirkle rules.
        valid = True
        # Initalize two bool variables.
        none_bool = False
        try:
            color = np.floor(tile/10)
            shape = tile % 10
    
            # Step 1 is checking if the position on the board is empty
            step1 = False
            if board[position] == 0:
                step1 = True
                
            # Step 2 is checking if there exists at least one neighbour //that could potentially match i.e. tile is connected to the rest of the board.    
            step2 = False
            neighbours = self.find_neighbours(board,position)
            if step1:
                for i in range(4):
                    if neighbours[i,0] != 0:
                        step2 = True
                        
            # Commons will store the matching color or tile and a marker in each row. Each row represents one search directions i.e. NSWE.
            commons = np.zeros((4,2))
            # Validator will hold bool values that are stored after performing checks.
            validator = np.zeros((4,6))
            
            if step2:
                # Looping over the four search directions i.e. NSWE.
                for i in range(4):
                    # For each search direction set current lists and reassign later.
                    current_row = neighbours[i,:]
                    current_validator = validator[i,:]
                    
                    if current_row[0] == 0:
                        # If the closest neighbour is zero i.e. no tile in that spot the validator is set to all ones i.e. all true.
                        current_validator = [1,1,1,1,1,1]
                    else:
                        # If not direct neighbours color and shape are stored.
                        current_closest_color = np.floor(current_row[0]/10)
                        current_closest_shape = current_row[0]%10
                        
                        # Initalize variabales for following checks.
                        common_attribute = [0,0]
                        bool1 = False
                        bool2 = False
                        
                        if color == current_closest_color:
                            # Check if color of tile to place is the same as color of direct neighbour.
                            bool1 = True
                        
                        if shape == current_closest_shape:
                            # Check if shape of tile to place is the same as shape of direct neighbour.
                            bool2 = True
                            
                        if bool1 ^ bool2:
                            # If XOR one of the two conditons before is true validator in closest position is set to true.
                            current_validator[0] = True                          
                            # In case of XOR -> true, the attribute that the tile to place and the closest neighbour share is stored together with marker 1 or 2-
                            # to store wether the color or shape matched.
                            if bool1:
                                common_attribute = [1,color]
                            if bool2:
                                common_attribute = [2,shape]
                        # Result of the checks is stored in commons array.
                        commons[i,:] = common_attribute
                        
                        
                        # Next loop over the neighbours with increasing distance.
                        for j in range(1,6):
                            
                            if current_row[j] != 0:
                                # If the neighbours at current distance is not zero i.e. empty square,- 
                                #get its shape and color and store in the current_closest variables.
                                current_closest_color = np.floor(current_row[j]/10)
                                current_closest_shape = current_row[j]%10
                                
                                if common_attribute[0] == 1:
                                    # In case that common_attribute of the tile to play and its closest neighbour was their color, check if the tile at current-
                                    # distance has the same color and if it is not the same shape i.e. same tile.
                                    if color == current_closest_color and shape != current_closest_shape:
                                        # In that case set validator at current position to true.
                                        current_validator[j] = True
                                    # Same check as above but for common_attribute being the shape.
                                elif common_attribute[0] == 2:
                                    if shape == current_closest_shape and color != current_closest_color:
                                        current_validator[j] = True
                            
                            else:
                                # If the tile at current distance is zero i.e. empty square, set its validator to true aswell as all of the following neighbours.
                                for k in range(j,6):
                                    current_validator[k] = True
                                # In that case there is no need to search any longer.
                                break
                            
                    # Save current in validator array.
                    validator[i,:] = current_validator

            # Initalize the returned valid variable aswell as a flag for nested loop.                
            valid = True
            flag = False
            
            # Loop over all the entries in the validator.
            for i in range(4):
                for j in range(6):
                    if validator[i,j] == False:
                        # If validator is false at any position set valid to false.
                        valid = False
                        flag = True              
                        break
                    
                if flag:
                    break
        
        # Except handles NoneType inputs
        except:
            if type(tile) == None:
                none_bool = True          
        
        # Check if board is empty i.e. first move.
        if np.all(board==0):
            valid = True
        
        # Check if NoneType Exception occured.
        if none_bool:
            valid = False

        # Additional check that handles tiles being placed in the middle of existing neighbours to ensure no double tiles in a row
        all_row_neighbours_in_one_list = []
        all_col_neighbours_in_one_list = []
        for idx,row in enumerate(neighbours):
            if idx > 1:
                pass
            else:
                for tile in row:
                    if tile != 0:
                        all_row_neighbours_in_one_list.append(tile)
                    else:
                        break
        
        for idx,col in enumerate(neighbours):
            if idx < 2:
                pass
            else:
                for tile in col:
                        if tile != 0:
                            all_col_neighbours_in_one_list.append(tile)
                        else:
                            break

        if len(all_col_neighbours_in_one_list) != len(set(all_col_neighbours_in_one_list)):
            valid = False
        elif len(all_row_neighbours_in_one_list) != len(set(all_row_neighbours_in_one_list)):
            valid = False
    
        # Additional check to verify if tile is placed in between existing tiles:
        flag = True

        try:
            direct_neighbours = self.find_direct_neighbours_and_positions(board,position)
            
        except IndexError:
            flag = False

        if flag:
            i=0
            while i < len(direct_neighbours):
                if direct_neighbours[i][0] == 0:
                    direct_neighbours.pop(i)
                else:
                    i+=1

            i = 0
            
            while i < len(direct_neighbours):
                direct_neighbours[i][0] = int(direct_neighbours[i][0])
                i+=1

            
            # Check for two neighbours if its not a corner move
            if len(direct_neighbours) == 2:
                # Check if they are vertical neighbours
                if direct_neighbours[0][1][0]==0 and direct_neighbours[1][1][0]==0:
                    first_color = np.floor(direct_neighbours[0][0]/10)
                    first_shape = direct_neighbours[0][0] % 10

                    second_color = np.floor(direct_neighbours[1][0]/10)
                    second_shape = direct_neighbours[1][0] % 10
                    # Check if neither color nor shape matches
                    if first_color!=second_color and first_shape!=second_shape:
                        valid = False
                        
                # Check if they are horizontal neighbours
                elif direct_neighbours[0][1][1]==0 and direct_neighbours[1][1][1]==0:
                    # Check if neither color nor shape matches
                    first_color = np.floor(direct_neighbours[0][0]/10)
                    first_shape = direct_neighbours[0][0] % 10

                    second_color = np.floor(direct_neighbours[1][0]/10)
                    second_shape = direct_neighbours[1][0] % 10
                    if first_color!=second_color and first_shape!=second_shape:
                        valid = False

            # Check for three neighbours
            if len(direct_neighbours) == 3:
                # Find the two neighbours that are in same row as tile placed
                horz_neighbours = []
                vert_neighbours = []

                for neighbour in direct_neighbours:
                    if neighbour[1][0] == 0:
                        horz_neighbours.append(neighbour)
                    elif neighbour[1][1] == 0:
                        vert_neighbours.append(neighbour)

                # If the same line neighbours dont match in either shape or color set valid to False
                if len(horz_neighbours) == 2:
                    first_color = np.floor(horz_neighbours[0][0]/10)
                    first_shape = horz_neighbours[0][0] % 10

                    second_color = np.floor(horz_neighbours[1][0]/10)
                    second_shape = horz_neighbours[1][0] % 10
                    if first_color!=second_color and first_shape!=second_shape:
                        valid = False

                elif len(vert_neighbours) == 2:
                    first_color = np.floor(vert_neighbours[0][0]/10)
                    first_shape = vert_neighbours[0][0] % 10

                    second_color = np.floor(vert_neighbours[1][0]/10)
                    second_shape = vert_neighbours[1][0] % 10
                    if first_color!=second_color and first_shape!=second_shape:
                        valid = False   
            
            if len(direct_neighbours) == 4:
                # Find the two neighbours that are in same row as tile placed
                horz_neighbours = []
                vert_neighbours = []

                for neighbour in direct_neighbours:
                    if neighbour[1][0] == 0:
                        horz_neighbours.append(neighbour)
                    elif neighbour[1][1] == 0:
                        vert_neighbours.append(neighbour)
                    
                
                if len(horz_neighbours) == 2:
                    
                    first_color = np.floor(horz_neighbours[0][0]/10)
                    first_shape = horz_neighbours[0][0] % 10

                    second_color = np.floor(horz_neighbours[1][0]/10)
                    second_shape = horz_neighbours[1][0] % 10
                    if first_color!=second_color and first_shape!=second_shape:
                        valid = False

                if len(vert_neighbours) == 2:
                  
                    first_color = np.floor(vert_neighbours[0][0]/10)
                    first_shape = vert_neighbours[0][0] % 10

                    second_color = np.floor(vert_neighbours[1][0]/10)
                    second_shape = vert_neighbours[1][0] % 10
                    if first_color!=second_color and first_shape!=second_shape:
                        valid = False  
        return valid    

    
    def additonal_game_flow_check(self,board,selected_tile,i,j,turn_memory):
        # When more than 1 piece is placed during a single turn, additional rules apply. Validity considering these moves is checked here.
        
        is_valid = True

        if len(turn_memory) == 1 and selected_tile is not None:
            first_piece_color = int(str((turn_memory[0])[0])[0])
            first_piece_shape = int(str((turn_memory[0])[0])[1])
            
            second_piece_color = int(str(selected_tile)[0])
            second_piece_shape = int(str(selected_tile)[1])
            
            if first_piece_color == second_piece_color:
                ident_index = 0
                ident = first_piece_color
            elif first_piece_shape == second_piece_shape:
                ident_index = 1
                ident = first_piece_shape
            
            try:
                if ident != int(str(selected_tile)[ident_index]):
                    is_valid = False
                else:
                    pass
            except:
                is_valid = False
                
        # Additional check using the turn memory to make sure that if multiple tiles are played they all share the same color or shape.
        if len(turn_memory) >= 2 and selected_tile is not None:
            
          
            first_piece_color = int(str((turn_memory[0])[0])[0])
            first_piece_shape = int(str((turn_memory[0])[0])[1])

            second_piece_color = int(str((turn_memory[1])[0])[0])
            second_piece_shape = int(str((turn_memory[1])[0])[1])
                    
            if first_piece_color == second_piece_color:
                ident_index = 0
                ident = first_piece_color
            elif first_piece_shape == second_piece_shape:
                ident_index = 1
                ident = first_piece_shape
                         
            if ident != int(str(selected_tile)[ident_index]):
                is_valid = False
        

        # Additional check using turn memory to completly outrule "cornering" when multiple tiles are played by making sure tiles are all in the same line.
        if len(turn_memory) >= 2 and type(selected_tile)!= None:
           
            first_piece_x = turn_memory[0][1]
            first_piece_y = turn_memory[0][2]
            
            second_piece_x = turn_memory[1][1]
            second_piece_y = turn_memory[1][2]
            
            if first_piece_x == second_piece_x:
                if first_piece_x != i:
                    is_valid = False
            elif first_piece_y == second_piece_y:
                if first_piece_y != j:
                    is_valid = False
            else:
                is_valid = False
                    
        if len(turn_memory) == 1 and type(selected_tile)!= None:
            
            first_piece_x = turn_memory[0][1]
            first_piece_y = turn_memory[0][2]
            
            second_piece_x = i
            second_piece_y = j
            
            if first_piece_x == second_piece_x:
                if first_piece_x != i:
                    is_valid = False
            elif first_piece_y == second_piece_y:
                if first_piece_y != j:
                    is_valid = False
            else:
                is_valid = False
                
                
        if len(turn_memory) >= 1 and selected_tile is not None:
            first_piece_x = turn_memory[0][1]
            first_piece_y = turn_memory[0][2]
            
            second_piece_x = i
            second_piece_y = j
            

            
            if first_piece_x == second_piece_x:
                if abs(first_piece_y-second_piece_y) != 1:
                    
                    if second_piece_y > first_piece_y:
                        
                        for i in range(1,abs(first_piece_y-second_piece_y)):
                            if board[first_piece_x,first_piece_y+i] == 0:
                                is_valid = False
                    
                    
                    if second_piece_y < first_piece_y:
                        
                        for i in range(1,abs(first_piece_y-second_piece_y)):
                            if board[first_piece_x,second_piece_y+i] == 0:
                                is_valid = False
            
            elif first_piece_y == second_piece_y:
              
                if abs(first_piece_x-second_piece_x) != 1:
                    
                    if second_piece_x > first_piece_x:
                        
                        for i in range(1,abs(first_piece_x-second_piece_x)):
                            if board[first_piece_x+i,first_piece_y] == 0:
                                is_valid = False
                    
                    
                    if second_piece_x < first_piece_x:
    
                        for i in range(1,abs(first_piece_x-second_piece_x)):
                            if board[second_piece_x+i,first_piece_y] == 0:
                                is_valid = False

        # First move exception:                          
        if np.all(board==0):
            is_valid = True
            
            
        return is_valid        
    
    # utility
    def find_direct_neighbours_and_positions(self,board,position):
        direct_neighbours = []
        directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]

        for i in range(4):
            direct_neighbours.append([board[position[0]+directions[i][0], position[1]+directions[i][1]],directions[i]])
        
        return direct_neighbours
    # utility
    def find_direct_neighbours(self,board,position):
        
        direct_neighbours = np.zeros((1,4))
        
        directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]
        
        for i in range(4):
            direct_neighbours[0,i] = board[position[0]+directions[i][0], position[1]+directions[i][1]]
            
        return direct_neighbours
        
    # utility
    def find_neighbours(self,board, position):
        # Get the 4 neighbouring positions to one specific position
        neighbours = np.zeros((4, 6), dtype=int)
    
        # Define the four directions: up, down, left, right
        directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]
    
        for i in range(4):
            for j in range(6):
                # Calculate the coordinates of the neighbor
                neighbour_row = position[0] + directions[i][0] * (j + 1)
                neighbour_col = position[1] + directions[i][1] * (j + 1)
    
                # Check if the neighbor coordinates are within bounds
                if 0 <= neighbour_row < board.shape[0] and 0 <= neighbour_col < board.shape[1]:
                    neighbours[i, j] = board[neighbour_row, neighbour_col]
                else:
                    neighbours[i, j] = 0
    
        return neighbours

    # Calculates a moves score
    def calculate_score(self,board, moves):
        
        directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]
        moves_coords = []
        
        new_lines = [0,0,0,0]
        count_new_lines = 0
        
        already_checked = np.zeros((board.shape))
 
        for i in range(len(moves)):
            moves_coords.append([moves[i][1],moves[i][2]])
            
        for i in range(len(moves)):
            already_checked[moves_coords[i][0], moves_coords[i][1]] = 1
        
        for i in range(len(moves)):
            new_lines = [0,0,0,0]
            current_neighbours = self.find_neighbours(board,moves_coords[i])
            
            for j in range(4):
                for k in range(6):
                    
                    if current_neighbours[j,k] != 0 and not already_checked[moves_coords[i][0]+ (k+1)*directions[j][0], moves_coords[i][1]+ (k+1) * directions[j][1]]:    
                        already_checked[moves_coords[i][0]+ (k+1)*directions[j][0], moves_coords[i][1]+ (k+1) * directions[j][1]] = 1
                    else:
                        break
  
            direct_neighbours = self.find_direct_neighbours(board,moves_coords[i])
  
            direct_neighbours_pos = [[moves_coords[i][0],moves_coords[i][1]+1], [moves_coords[i][0],moves_coords[i][1]-1],
                                      [moves_coords[i][0]-1,moves_coords[i][1]], [moves_coords[i][0]+1,moves_coords[i][1]]]
    
            for j in range(4):
                
                if direct_neighbours[0,j] != 0:  
                    new_lines[j] = 1

            if (new_lines[0] and new_lines[2]) or (new_lines[0] and new_lines[3]) or (new_lines[1] and new_lines[2]) or (new_lines[1] and new_lines[3]): 
                count_new_lines += 1
         
        qwirkles = 0 
        rows, cols = already_checked.shape
        
        for col in range(cols):
            consecutive_pieces = 0
            
            for row in range(rows):
                if already_checked[row,col] == 1:
                    consecutive_pieces += 1  

                    if consecutive_pieces == 6:    
                        qwirkles += 1
                else:
                    consecutive_pieces = 0
                    
                    
        for row in range(rows):
            consecutive_pieces = 0
        
            for col in range(cols):
                
                if already_checked[row,col] == 1:
                    consecutive_pieces += 1
                    
                    if consecutive_pieces == 6:    
                        qwirkles += 1
        
                else:
                    consecutive_pieces = 0
              
        board_score = np.count_nonzero(already_checked)
        qwirkle_bonus = qwirkles * 6
    
        total_score = (board_score + count_new_lines + qwirkle_bonus)
        return total_score
        
    def upsize_board(self,board,selected_tile,turn_memory,*args):
        # This method enlargens the board if necessary. That is the case when a tile is played on the edge of the board.
        if args:
            bot_moves = args[0]
        # Get size of old board.
   
        board_height = board.shape[0]
        board_width = board.shape[1]
        
        # Get all four old board edges.
        top_row = board[0,:]
        left_column = board[:,0]
        bottom_row = board[board_height-1,:]
        right_column = board[:,board_width-1]
        

        if board_height == 1 and board_width == 1:
            board = np.zeros((3,3))
            board[1,1] = selected_tile
            turn_memory[0][1] = 1
            turn_memory[0][2] = 1
        
        else:
            if not np.all(top_row==0):
                for i in range(len(turn_memory)):
                    turn_memory[i][1] = turn_memory[i][1] + 1   
                    if args:
                        try:
                            bot_moves[i][1] = bot_moves[i][1] + 1
                        except:
                            pass            
                board = np.vstack((np.zeros((1,board_width)),board))

            elif not np.all(bottom_row==0):
                board = np.vstack((board,np.zeros((1,board_width))))

            elif not np.all(left_column==0):
                for i in range(len(turn_memory)):
                    turn_memory[i][2] = turn_memory[i][2] + 1
                    if args:
                        bot_moves[i][2] = bot_moves[i][2] + 1
                board = np.hstack((np.zeros((board_height,1)),board))
 
            elif not np.all(right_column==0):
                board = np.hstack((board,np.zeros((board_height,1))))
   
        if args:
            return board,turn_memory,bot_moves

        return board,turn_memory 
        

    def switch_turn(self):
        # Switch to the next player's turn.   
        current_index = self.players.index(self.current_player)
        next_index = (current_index + 1) % len(self.players)
        self.current_player = self.players[next_index]
      

    def is_game_over(self):
        # Check if the game is over (e.g., no more valid moves or tiles).
        if len(self.bag.tiles) == 0 and all(len(player.hand) == 0 for player in self.players):
            return True
        return False
        
    def get_winner(self):
        # Determine the winner and return the player object.
        winner = None
        highest_score = -1
        for player in self.players:
            if player.score > highest_score:
                winner = player
                highest_score = player.score
            elif player.score == highest_score:
                winner = None            
        return winner
        

# Define the Player class
class Player:
    def __init__(self, name,id):
        self.name = name
        self.id = id
        self.hand = []
        self.bench_buttons = []
        self.image_list = []
        self.score = 0
        self.is_bot = None

# Define bot classs, is child of player
class Bot(Player):


    def __init__(self,name,qwirkle_game,id):
        super().__init__(name,id)

        self.is_bot = True
        self.game = qwirkle_game

        self.turn_memory = []
        self.id = id
        self.total_thinking_time = 0

    def get_first_moves(self,board,hand):

        first_moves = []

        m = board.shape[0]
        n = board.shape[1]
        
        for i in range(m):
            for j in range(n):
                if board[i,j]==0 and self.has_neighbours(board,i,j,m,n):
                    for k in range(len(hand)):
                        if QwirkleGame.is_valid_move(self.game,board,hand[k],(i,j)):    

                            if QwirkleGame.additonal_game_flow_check(self.game,board,hand[k],i,j,self.turn_memory):
                                    first_moves.append([hand[k],i,j])    
                            elif len(self.turn_memory) == 0:
                                first_moves.append([hand[k],i,j])
       
        return first_moves

    def has_neighbours(self,board,i,j,m,n):
        xrange = range(i-1,i+2)
        yrange = range(j-1,j+2)

        if i!=0 and i!=m and j!=0 and j!=n:
            for x in xrange:
                for y in yrange:
                    try:
                        if board[x,y]!=0:
                                return True
                    except:
                        pass        
        else:
            return True

    def make_move(self,board,move,tiles):
        tile,i,j = move[0],move[1],move[2]
        self.turn_memory.append(move)
        
        for idx,piece in enumerate(tiles):
            if piece == tile:
                tiles.pop(idx)
                break

        board[i,j] = tile 
        board,self.turn_memory = QwirkleGame.upsize_board(self.game,board,tile,self.turn_memory)            
                
        return board,tiles
    
    


    def create_static_board(self,input_board):
        m,n = input_board.shape

        for i in range(4):
            input_board = np.vstack((input_board,np.zeros((1,n))))
            input_board = np.vstack(((np.zeros((1,n))),input_board))

        m,n = input_board.shape
       
        for i in range(4):
            input_board = np.hstack((input_board,np.zeros((m,1))))
            input_board = np.hstack(((np.zeros((m,1))),input_board))

        return input_board
        
    # Unfinished, purpose: parallelize the tree search
    """   def all_parallel(self,input_board,hand):
        resulting_boards = []
        hands = []
        best_scores = []
        best_movess = []
        tasks = [(input_board,hand,0,),(input_board,hand,1),(input_board,hand,2),(input_board,hand,3),(input_board,hand,4),(input_board,hand,5)]
        with Pool() as pool:
            for result in pool.starmap(self.single_parallel,tasks):
                print(result)
        server = pp.Server()
        j1 = server.submit(self.single_parallel,tasks[0],(self.create_static_board,self.get_first_moves,),('Bot',))
        r1 = j1()
        
              
    def single_parallel(self,input_board,hand,idx):
        self.turn_memory = []

        best_moves = None
        best_score = 0
        i = 0
        print('ouf')
        working_board = self.create_static_board(input_board)
        print(working_board)
        print(hand[idx])
        try:
            first_moves = self.get_first_moves(working_board,[hand[idx]])
        except: 
            return
        
        a_idx = 0
        while True:
            tiles = hand
            b_idx = 0
            
            try:
                i+=1
                this_move = first_moves[a_idx]
                working_board,tiles = self.make_move(working_board,this_move,tiles)
                a_idx += 1
                b_moves = self.get_first_moves(working_board,tiles)
                
            except IndexError:
                this_score = self.game.calculate_score(working_board,self.turn_memory)
                if this_score > best_score:
                    best_moves = self.turn_memory.copy()
                    best_score = this_score

                break

            while True:
                c_idx = 0
                try:   
                    i+=1
                    this_b_move = b_moves[b_idx]
                    working_board,tiles = self.make_move(working_board,this_b_move,tiles)
                    b_idx += 1

                    c_moves = self.get_first_moves(working_board,tiles)
                    
                except IndexError:
                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                    if this_score > best_score:
                        best_moves = self.turn_memory.copy()
                        best_score = this_score

                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                    self.turn_memory.pop()       

                    break

                while True:
                    d_idx = 0
                    try:
                        i+=1
                        this_c_move = c_moves[c_idx]
                        working_board,tiles = self.make_move(working_board,this_c_move,tiles)
                        c_idx += 1

                        d_moves = self.get_first_moves(working_board,tiles)
                            
                    except IndexError:
                        this_score = self.game.calculate_score(working_board,self.turn_memory)
                        if this_score > best_score:
                            best_moves = self.turn_memory.copy()
                            best_score = this_score

                        working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                        self.turn_memory.pop()
                        break

                    while True:
                        e_idx = 0
                        try:      
                            i+=1 
                            this_d_move = d_moves[d_idx]
                            working_board,tiles = self.make_move(working_board,this_d_move,tiles)
                            d_idx += 1

                            e_moves = self.get_first_moves(working_board,tiles)
                                
                        except IndexError:
                            this_score = self.game.calculate_score(working_board,self.turn_memory)
                            if this_score > best_score:
                                best_moves = self.turn_memory.copy()
                                best_score = this_score

                            working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                            self.turn_memory.pop()

                            break

                        while True:
                            f_idx = 0
                            try:
                                i+=1
                                this_e_move = e_moves[e_idx]
                                working_board,tiles = self.make_move(working_board,this_e_move,tiles)
                                e_idx += 1

                                f_moves = self.get_first_moves(working_board,tiles)
                                    
                            except IndexError:
                                this_score = self.game.calculate_score(working_board,self.turn_memory)
                                if this_score > best_score:
                                    best_moves = self.turn_memory.copy()
                                    best_score = this_score

                                working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                self.turn_memory.pop()

                                break

                            while True:
                                try: 
                                    i+=1
                                    this_f_move = f_moves[f_idx]
                                    working_board,tiles = self.make_move(working_board,this_f_move,tiles)
                                    f_idx += 1
                                        
                                except IndexError:
                                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                                    if this_score > best_score:
                                        best_moves = self.turn_memory.copy()
                                        best_score = this_score

                                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                    self.turn_memory.pop()
                                    break

        #print('The Bot considered ',i,' moves')
        working_board = self.create_static_board(input_board)

        for move in best_moves:
            working_board,hand = self.make_move(working_board,move,hand)

        resulting_board,best_moves = self.ddownsize_board(working_board,best_moves)     
       
        resulting_board,best_moves = self.upsize_board(resulting_board,best_moves) 
                  
        return #resulting_board,hand,best_score,best_moves 
    """
        
    def parallel_move_search(self,input_board,hand):
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            moves = self.get_first_moves(input_board,hand)

            print(f'There are {len(moves)} possible first moves')

            process_args = [(move,input_board,hand) for move in moves]
            
            results = pool.starmap(self.fast_get_best_move, process_args)
    
        pool.close()
        pool.join()

        # Extract the best result
        best_score = 0
        best_moves = []
       
        for r_b, r_h, score, moves in results:
            if score > best_score:
                best_score = score
                best_moves = moves
                best_board = r_b
                best_hand = r_h



        return best_board, best_hand, best_score, best_moves
    
    def fast_get_best_move(self,first_move,input_board,hand):

        self.turn_memory = []

        best_moves = None
        best_score = 0
        i = 0
        
        working_board = self.create_static_board(input_board)
    
        a_idx = 0

        if np.all(working_board==0):


            j=random.randint(0,5)
            move=[hand[j],0,0]

            working_board,tiles = self.make_move(working_board,move,hand)
            this_score = self.game.calculate_score(working_board,self.turn_memory)

            resulting_board,best_moves = self.ddownsize_board(working_board,[move])     
            resulting_board,best_moves = self.upsize_board(resulting_board,[move])      

            return resulting_board,hand,this_score,best_moves


        while True:
            tiles = hand
            b_idx = 0

            
            try:
                i+=1
                self.turn_memory = []
                this_move = [first_move][a_idx]

                working_board,tiles = self.make_move(working_board,this_move,tiles)

                a_idx += 1

                b_moves = self.get_first_moves(working_board,tiles)
                
          
            except IndexError:
                this_score = self.game.calculate_score(working_board,self.turn_memory)
                if this_score > best_score:
                    best_moves = self.turn_memory.copy()
                    best_score = this_score

                break

            while True:
                c_idx = 0
                try:   
                    i+=1
                    this_b_move = b_moves[b_idx]
                    working_board,tiles = self.make_move(working_board,this_b_move,tiles)
                    b_idx += 1

                    c_moves = self.get_first_moves(working_board,tiles)
                    
                except IndexError:
                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                    if this_score > best_score:
                        best_moves = self.turn_memory.copy()
                        best_score = this_score

                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                    self.turn_memory.pop()       

                    break

                while True:
                    d_idx = 0
                    try:
                        i+=1
                        this_c_move = c_moves[c_idx]
                        working_board,tiles = self.make_move(working_board,this_c_move,tiles)
                        c_idx += 1

                        d_moves = self.get_first_moves(working_board,tiles)
                            
                    except IndexError:
                        this_score = self.game.calculate_score(working_board,self.turn_memory)
                        if this_score > best_score:
                            best_moves = self.turn_memory.copy()
                            best_score = this_score

                        working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                        self.turn_memory.pop()
                        break

                    while True:
                        e_idx = 0
                        try:      
                            i+=1 
                            this_d_move = d_moves[d_idx]
                            working_board,tiles = self.make_move(working_board,this_d_move,tiles)
                            d_idx += 1

                            e_moves = self.get_first_moves(working_board,tiles)
                                
                        except IndexError:
                            this_score = self.game.calculate_score(working_board,self.turn_memory)
                            if this_score > best_score:
                                best_moves = self.turn_memory.copy()
                                best_score = this_score

                            working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                            self.turn_memory.pop()

                            break

                        while True:
                            f_idx = 0
                            try:
                                i+=1
                                this_e_move = e_moves[e_idx]
                                working_board,tiles = self.make_move(working_board,this_e_move,tiles)
                                e_idx += 1

                                f_moves = self.get_first_moves(working_board,tiles)
                                    
                            except IndexError:
                                this_score = self.game.calculate_score(working_board,self.turn_memory)
                                if this_score > best_score:
                                    best_moves = self.turn_memory.copy()
                                    best_score = this_score

                                working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                self.turn_memory.pop()

                                break

                            while True:
                                try: 
                                    i+=1
                                    this_f_move = f_moves[f_idx]
                                    working_board,tiles = self.make_move(working_board,this_f_move,tiles)
                                    f_idx += 1
                                        
                                except IndexError:
                                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                                    if this_score > best_score:
                                        best_moves = self.turn_memory.copy()
                                        best_score = this_score

                                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                    self.turn_memory.pop()
                                    break

        print('The Bot considered ',i,' moves')
        
        working_board = self.create_static_board(input_board)

        for move in best_moves:
            working_board,hand = self.make_move(working_board,move,hand)


        resulting_board,best_moves = self.ddownsize_board(working_board,best_moves)     
        resulting_board,best_moves = self.upsize_board(resulting_board,best_moves) 
   
        return resulting_board,hand,best_score,best_moves 

    def get_best_move(self,input_board,hand):

        self.turn_memory = []

        best_moves = None
        best_score = 0
        i = 0
        
        working_board = self.create_static_board(input_board)
    
        a_idx = 0
        first_moves = self.get_first_moves(working_board,hand)
        
        if np.all(working_board==0):

            j=random.randint(0,5)
            move=[hand[j],0,0]

            working_board,tiles = self.make_move(working_board,move,hand)
            this_score = self.game.calculate_score(working_board,self.turn_memory)

            resulting_board,best_moves = self.ddownsize_board(working_board,[move])     
            resulting_board,best_moves = self.upsize_board(resulting_board,[move])      

            return resulting_board,hand,this_score,best_moves


        while True:
            tiles = hand
            b_idx = 0
            
            try:
                i+=1
                self.turn_memory = []
                this_move = first_moves[a_idx]
                
                working_board,tiles = self.make_move(working_board,this_move,tiles)
                a_idx += 1

                b_moves = self.get_first_moves(working_board,tiles)
                
          
            except IndexError:
                this_score = self.game.calculate_score(working_board,self.turn_memory)
                if this_score > best_score:
                    best_moves = self.turn_memory.copy()
                    best_score = this_score

                break

            while True:
                c_idx = 0
                try:   
                    i+=1
                    this_b_move = b_moves[b_idx]
                    working_board,tiles = self.make_move(working_board,this_b_move,tiles)
                    b_idx += 1

                    c_moves = self.get_first_moves(working_board,tiles)
                    
                except IndexError:
                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                    if this_score > best_score:
                        best_moves = self.turn_memory.copy()
                        best_score = this_score

                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                    self.turn_memory.pop()       

                    break

                while True:
                    d_idx = 0
                    try:
                        i+=1
                        this_c_move = c_moves[c_idx]
                        working_board,tiles = self.make_move(working_board,this_c_move,tiles)
                        c_idx += 1

                        d_moves = self.get_first_moves(working_board,tiles)
                            
                    except IndexError:
                        this_score = self.game.calculate_score(working_board,self.turn_memory)
                        if this_score > best_score:
                            best_moves = self.turn_memory.copy()
                            best_score = this_score

                        working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                        self.turn_memory.pop()
                        break

                    while True:
                        e_idx = 0
                        try:      
                            i+=1 
                            this_d_move = d_moves[d_idx]
                            working_board,tiles = self.make_move(working_board,this_d_move,tiles)
                            d_idx += 1

                            e_moves = self.get_first_moves(working_board,tiles)
                                
                        except IndexError:
                            this_score = self.game.calculate_score(working_board,self.turn_memory)
                            if this_score > best_score:
                                best_moves = self.turn_memory.copy()
                                best_score = this_score

                            working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                            self.turn_memory.pop()

                            break

                        while True:
                            f_idx = 0
                            try:
                                i+=1
                                this_e_move = e_moves[e_idx]
                                working_board,tiles = self.make_move(working_board,this_e_move,tiles)
                                e_idx += 1

                                f_moves = self.get_first_moves(working_board,tiles)
                                    
                            except IndexError:
                                this_score = self.game.calculate_score(working_board,self.turn_memory)
                                if this_score > best_score:
                                    best_moves = self.turn_memory.copy()
                                    best_score = this_score

                                working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                self.turn_memory.pop()

                                break

                            while True:
                                try: 
                                    i+=1
                                    this_f_move = f_moves[f_idx]
                                    working_board,tiles = self.make_move(working_board,this_f_move,tiles)
                                    f_idx += 1
                                        
                                except IndexError:
                                    this_score = self.game.calculate_score(working_board,self.turn_memory)
                                    if this_score > best_score:
                                        best_moves = self.turn_memory.copy()
                                        best_score = this_score

                                    working_board,tiles = self.undo_last_move(working_board,self.turn_memory[-1],tiles)
                                    self.turn_memory.pop()
                                    break

        print('The Bot considered ',i,' moves')
        working_board = self.create_static_board(input_board)

        for move in best_moves:
            working_board,hand = self.make_move(working_board,move,hand)

        resulting_board,best_moves = self.ddownsize_board(working_board,best_moves)     
        resulting_board,best_moves = self.upsize_board(resulting_board,best_moves) 
                  
        return resulting_board,hand,best_score,best_moves
                        
    def legal(self,move):
        legal = False
        if QwirkleGame.is_valid_move(self.game,move[0],(move[1],move[2])):

            if self.turn_memory:
                if QwirkleGame.additonal_game_flow_check(self.game,move[0],move[1],move[2],self.turn_memory):
                    legal = True
            else:
                legal = True
        return legal

    def undo_last_move(self,board,move,tiles):
        tile,i,j = move
        board[i,j] = 0
        tiles.append(tile)
        return board,tiles

    def ddownsize_board(self,board,moves):
        m = board.shape[0]
        n = board.shape[1]
        col_counter = 0
        row_counter = 0
        row_min = m
        col_min = n

        for move in moves:
            if row_min > move[1]:
                row_min = move[1]
            if col_min > move[2]:
                col_min = move[2]

        for row in reversed(range(m)):

            if np.all(board[row,:]==0):
                    board = np.delete(board,row,0) 
                    if row < row_min:
                        row_counter+=1
        
        for col in reversed(range(n)):
            if np.all(board[:,col]==0):
                board = np.delete(board,col,1)
                if col < col_min:
                    col_counter += 1

        for move in moves:
            move[1] -= row_counter
            move[2] -= col_counter

        return board,moves
      
    def upsize_board(self,board,moves):

        for move in moves:
            move[1] += 1
            move[2] += 1

        # Get size of old board.
        board_height = board.shape[0]
        board_width = board.shape[1]
        
        # Get all four old board edges.
        top_row = board[0,:]
        left_column = board[:,0]
        bottom_row = board[board_height-1,:]
        right_column = board[:,board_width-1]

        if not np.all(top_row==0):
            board = np.vstack((np.zeros((1,board_width)),board))

        if not np.all(bottom_row==0):
            board = np.vstack((board,np.zeros((1,board_width))))

        board_height = board.shape[0]
        board_width = board.shape[1]

        if not np.all(left_column==0):
            board = np.hstack((np.zeros((board_height,1)),board))

        if not np.all(right_column==0):
            board = np.hstack((board,np.zeros((board_height,1))))

        return board,moves

  
# Define the Bag class
class Bag:
    def __init__(self):
        #Initialize the bag with all the tiles.
         self.tiles = [11, 12, 13, 14, 15, 16, 21, 22, 23, 24, 25, 26, 31, 32, 33, 34, 35, 36, 41, 42, 43, 44, 45, 46, 51, 52, 53, 54, 55, 56, 61, 62, 63, 64, 65, 66] * 3
         #self.tiles = [11, 12, 13, 14, 15, 16, 21, 22, 23, 24, 25, 26, 31, 32]
    def draw_tile(self):
        #Draw random tiles
        rnd = random.randint(0,len(self.tiles)-1)
        tile = self.tiles[rnd]
        self.tiles.pop(rnd)
        return tile