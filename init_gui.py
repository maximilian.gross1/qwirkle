# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 21:32:03 2023

@author: Max
"""
from tkinter import ttk
from tkinter import font
import tkinter as tk
from PIL import ImageTk, Image

from qwirkle_gui import QwirkleGUI




class Qwirkle_init_GUI:
    
    def __init__(self):
        
        # Inititate window
        self.window = tk.Tk()
        self.window.title("Qwirkle Initialization")
        self.window.geometry("800x700")
        
        self.label_font = font.Font(family="Helvetica", size=16)
        
        # Initiate properties
        self.name_entries = []
        self.name_list = []
        self.name_labels = []
        
        # Initiate methods
        self.comboboxes()
        self.buttons()
        self.labels()
        
        # Initiate mainloop
        self.window.mainloop()
        
        
    def buttons(self):  
        
        # start_game_button = tk.Button(width=20, height=10,bg="green",text="START")
        # start_game_button.place(x=300,y=300)
        
        self.confirm_button = tk.Button(text="Confirm # of players", command = lambda: self.create_player_names())
        self.confirm_button.place(x=180,y=197)
        
    def labels(self):
        
        self.logo_image = Image.open("logo.jpg")
        self.logo_image = self.logo_image.resize((200,100),Image.LANCZOS)
        self.logo_image = ImageTk.PhotoImage(self.logo_image)
        
        logo_label = tk.Label(image=self.logo_image)
        logo_label.place(x=50,y=10)
        
        
        self.header = tk.Label(text = "Select number of Players", font=self.label_font)
        self.header.place(x=30,y=150)
        
        
        
        
    def comboboxes(self):
    
        self.select_num_of_players = ttk.Combobox(state="readonly", values=[1,2,3,4])
        self.select_num_of_players.place(x=30,y=200)
        
        
    def create_player_names(self):
        
        
        self.num_of_players = int(self.select_num_of_players.get())
        
        for i in range(self.num_of_players):
            
            entry = tk.Entry()
            entry.place(x=150,y=50*i+200,width=70)
            self.name_entries.append(entry)
            
            self.name_label = tk.Label(text=f"Enter Player {i+1} Name:")
            self.name_label.place(x=30,y=50*i+200)
            self.name_labels.append(self.name_label)
        
        self.select_num_of_players.destroy()
        
        self.confirm_button.config(text="Confirm Names", command= lambda: self.create_start_game_button()) 
        self.confirm_button.place(x=223,y=153)
        self.header.config(text="Enter player names")
        
        
        
            
    def create_start_game_button(self):
        
        # self.confirm_button.destroy()
        
        for i in range(len(self.name_entries)):
            self.name_list.append(self.name_entries[i].get())
            self.name_entries[i].destroy()
            self.name_labels[i].config(text=f"Player {i+1} Name: {self.name_list[i]}")
        
        self.header.config(text="Click Start Game:")
        
        self.confirm_button.config(text="Start Game", bg="spring green", command= lambda: self.start_game())
        self.confirm_button.place(x=213,y=153)
        
    def start_game(self):
        self.window.destroy()
        qwirkle_gui = QwirkleGUI()
        
        
        
    
if __name__ == "__main__":
    qwirkle_init_gui = Qwirkle_init_GUI()