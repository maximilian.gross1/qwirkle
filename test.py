import multiprocessing
import time

# Define a function to calculate Fibonacci numbers using recursion
def fibonacci(n):
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

# Function to use multiprocessing to calculate Fibonacci numbers
def parallel_fibonacci(numbers):
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        results = pool.map(fibonacci, numbers)
    return results

if __name__ == '__main__':
    print(f'Sharing load between {multiprocessing.cpu_count()} cores')
    # Define a list of Fibonacci numbers to calculate
    numbers = [37 for i in range(30,42)]  # Increasing numbers will take longer

    # Timing the sequential execution
    start_time = time.time()
    sequential_results = [fibonacci(n) for n in numbers]
    sequential_duration = time.time() - start_time
    print("Sequential results:", sequential_results)
    print("Sequential duration: {:.2f} seconds".format(sequential_duration))

    # Timing the parallel execution
    start_time = time.time()
    parallel_results = parallel_fibonacci(numbers)
    parallel_duration = time.time() - start_time
    print("Parallel results:", parallel_results)
    print("Parallel duration: {:.2f} seconds".format(parallel_duration))